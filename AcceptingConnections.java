// AcceptingConnections DONE
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.SocketException;

public class AcceptingConnections extends Thread {
    private Server server;
    private boolean running;

    public AcceptingConnections(Server s) {
        this.setServer(s);
        this.setRunning(true);
    }

    @Override
    public void run() {
        try {
            while (isRunning()) {
                try {

                    // Nasuchiwanie i akceptowanie połączenia
                    getServer().setS(getServer().getSs().accept());

                    System.out.println("Accepted new connection");

                    // ToDo. 2.1
                    DataInputStream dataInputS = new DataInputStream(getServer().getS().getInputStream());
                    DataOutputStream dataOutputS = new DataOutputStream(getServer().getS().getOutputStream());
                    ClientHandler clientHandler = new ClientHandler(getServer().getS(), getServer().getClientNr(), dataInputS, dataOutputS, getServer());  // Utworzenie obiektu klasy ClientHandler
                    Thread t = new Thread(clientHandler);  // Przekazanie do nowego wątku
                    //


                    getServer().getClientHandlersHM().put(getServer().getClientNr(), clientHandler); // add this client to active clients list

                    t.start(); //uruchom wątek

                    System.out.println("Client nr.: " + getServer().getClientNr() + " joined server.");
                    getServer().setClientNr(getServer().getClientNr() + 1);
                }catch (SocketException e2){
                    //System.out.println("Server socket closed");
                    setRunning(false);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error while accepting connection");
        }

    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}

